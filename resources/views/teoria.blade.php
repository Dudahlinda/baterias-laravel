@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Teoria</h1>
        <hr>
        <h2>Descrição das pilhas:</h2>
        <p>
            <h5>Resistência e Resistência Interna:</h5>
A resistência é uma propriedade elétrica que descreve a oposição que um material oferece à passagem de corrente elétrica. É medida em ohms (Ω) e determina a dificuldade que os elétrons encontram para se movimentarem através de um condutor. A resistência interna, por sua vez, refere-se à resistência presente dentro de uma fonte de energia, como uma bateria ou uma célula. Essa resistência interna pode resultar na queda de tensão e na redução da eficiência da fonte ao fornecer corrente a um circuito externo.

<h5>Multímetro:</h5>
O multímetro é uma ferramenta de teste elétrico versátil e amplamente utilizada para medir grandezas elétricas, como tensão (voltagem), corrente e resistência. Ele possui diferentes escalas e modos de operação para se adequar às diversas medições em circuitos eletrônicos e elétricos.

<h5>Tensão, Tensão V e Tensão E:</h5>
A tensão, também conhecida como diferença de potencial, é uma medida da energia elétrica por unidade de carga em um circuito. Ela é responsável por impulsionar o fluxo de elétrons em um circuito, criando um movimento de corrente. A tensão é medida em volts (V).

"Tensão V" é uma forma comum de se referir à tensão aplicada ou medida em um componente específico ou ponto de interesse em um circuito.

"Tensão E" é frequentemente usada para representar a tensão da fonte de energia, como a tensão fornecida por uma bateria ou uma fonte de alimentação.

<h5>Resistor:</h5>
Um resistor é um componente eletrônico projetado para ter uma resistência elétrica específica. Ele é usado para controlar o fluxo de corrente em um circuito, limitar a corrente elétrica ou dividir a tensão. Os resistores são identificados por suas marcas de cores ou valores de resistência em ohms.

<h5>Vida útil de uma bateria:</h5>
A vida útil de uma bateria se refere ao período em que a bateria é capaz de fornecer energia de forma satisfatória para um dispositivo específico. Essa duração pode variar dependendo do tipo de bateria, do uso do dispositivo e das condições de armazenamento. Com o tempo e uso repetido, a capacidade da bateria diminui gradualmente até atingir um ponto em que a energia fornecida é insuficiente, e a bateria precisa ser recarregada ou substituída.

Em resumo, resistência é a oposição ao fluxo de corrente elétrica, enquanto a resistência interna refere-se à resistência presente em uma fonte de energia. O multímetro é uma ferramenta para medir grandezas elétricas. A tensão é a diferença de potencial elétrico, medida em volts. O resistor é um componente eletrônico que controla a corrente em um circuito. A vida útil de uma bateria é o período em que ela pode fornecer energia suficiente para um dispositivo.
        </p>
    </main>
    @endsection

@section('teoria')
<h4> teoria da página inicial</h4>
@endsection