@extends('templates.base')

@section('conteudo')
    <main>
        <h1 class="h1">Turma: 2D3 - Grupo 1</h1>
        <h2 class="h2">Participantes:</h2>
        <hr>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Matrículas</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>


             <tr>
                <td>0072523</td>
                <td>Maria Eduarda da Costa Cassiano</td>
                <td>Gerente</td>
            </tr>


             <tr>
                <td>0056611</td>
                <td>Jean Carlos Silva de Oliveira</td>
                <td>Editor html</td>
            </tr>

             <tr>
                <td>0073450</td>
                <td>Alice Fernandes da Silva</td>
                <td>Medição</td>
            </tr>
            <tr>
                <td>0072563</td>
                <td>Danilo de Castro Macedo</td>
                <td>Editor html</td>
        </tr>




            <tr>
                <td>0066057</td>
                <td>Thiago Bekrman Silva Rocha</td>
                <td>Medição</td>
            </tr>
           
        </table>
        <img class="grupo" src="imgs/estudantes.jpeg" alt="Componentes do grupo">
    </main>

    @endsection

    @section('rodape')
    <h4> Rodapé da página inicial</h4>
    @endsection
    
