@extends('templates.base')

@section('conteudo')

    <main>
        <h1>Procedimento</h1>
        <hr>
        <h2>Medições:</h2>
        <p>
            Durante a aula de eletroeletrônica/programação em automação do dia 14/07, realizamos as medições das tensões de cada bateria/pilha escolhida pelo professor para realização do trabalho, o primeiro
            passo foi medir as pilhas e logo depois as baterias utilizando o multímetro, logo depois começamos as medições das baterias/pilhas com o Registor e para medir com o Registor precisamos mudar de
            resistência para tensão, anotamos os resultados da medições e conseguimos concluir que a diferença que com o Registor a pilha/bateria descarrega(cai pra zero mais rápido) e da uma estabilidade.
        </p>
        <h3>Tabela de medições:</h3>
        <p>
            Usando o código java do professor como exemplo nós montamos um código capaz de preencher a tabela com as imformações de medição das pilhas e baterias. Para não precisarmos fazer o calculo manual das
            resistências internas das pilhas e baterias nós usamos o mesmo código para fazer o cálculo e automaticamente o imprimir na tabela com o resto das informações.
        </p>
    </main>
    @endsection

@section('procedimentos')
<h4> procedimentos da página inicial</h4>
@endsection

